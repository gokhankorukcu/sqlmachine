﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Net.Sockets;
using System.IO;

namespace SQLMachineCommandNS
{
    public class SQLMachineCommand
    {
        public String db, password, server_ip;
        public int port;
        private String request_xml = "";

        public String message;
        public Int64 last_insert_id_or_affected_rows;
        public Boolean hasrows;

        private static byte[] inStream = new byte[1000000];
        private TcpClient clientSocket = new TcpClient();

        public SQLMachineCommand(String db, String password, String server_ip = "127.0.0.1", int port = 9998)
        {
            this.db = db;
            this.password = password;
            this.server_ip = server_ip;
            this.port = port;

            try
            {
                clientSocket.Connect(server_ip, port);
            }
            catch (Exception e)
            {
                message = e.Message;
                return;
            }
            message = "OK";
        }

        ~SQLMachineCommand()
        {
            try
            {
                if (clientSocket.Connected)
                    close_connection();
            }
            catch (Exception e)
            {
                
            }
        }

        public Boolean close_connection()
        {
            request_xml = "<command>CLOSE-CONNECTION</command>\n";
            request_xml += "<db>" + XmlEscape(db) + "</db>\n";
            request_xml += "<password>" + XmlEscape(password) + "</password>\n";

            request_xml += "</root>";
            request_xml = "<?xml version=\"1.0\" encoding=\"UTF-16\"?>\n<root>\n" + request_xml;

            reset();

            NetworkStream serverStream = null;

            try
            {
                serverStream = clientSocket.GetStream();
                byte[] outStream = Encoding.Unicode.GetBytes(request_xml);
                serverStream.Write(outStream, 0, outStream.Length);
                serverStream.Flush();
            }
            catch (Exception ex)
            {
                request_xml = "";
                message = ex.Message;
                return false;
            }

            request_xml = "";
            string returndata = "";

            try
            {
                int len = serverStream.Read(inStream, 0, 1000000);
                returndata = Encoding.Unicode.GetString(inStream, 0, len);
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return false;
            }

            XmlDocument doc = new XmlDocument();
            try
            {
                doc.LoadXml(returndata);
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return false;
            }

            message = doc.SelectNodes("//root/message")[0].InnerText;

            try
            {
                if (clientSocket.Connected)
                    clientSocket.Close();
            }
            catch (Exception e)
            {
                message = e.Message;
                return false;
            }

            if (message == "OK")
                return true;
            return false;
        }

        private string XmlEscape(string unescaped)
        {
            XmlDocument doc = new XmlDocument();
            XmlNode node = doc.CreateElement("root");
            node.InnerText = unescaped;
            return node.InnerXml;
        }

        private string XmlUnescape(string escaped)
        {
            XmlDocument doc = new XmlDocument();
            XmlNode node = doc.CreateElement("root");
            node.InnerXml = escaped;
            return node.InnerText;
        }

        public bool execute(String command, Boolean noreturn = false)
        {
            request_xml = "<command>" + XmlEscape(command) + "</command>\n";
            request_xml += "<db>" + XmlEscape(db) + "</db>\n";
            request_xml += "<password>" + XmlEscape(password) + "</password>\n";
            if (noreturn)
                request_xml += "<noreturn>1</noreturn>\n";

            request_xml += "</root>";
            request_xml = "<?xml version=\"1.0\" encoding=\"UTF-16\"?>\n<root>\n" + request_xml;

            reset();
           
            NetworkStream serverStream = null;

            try
            {
                serverStream = clientSocket.GetStream();
                byte[] outStream = Encoding.Unicode.GetBytes(request_xml);
                serverStream.Write(outStream, 0, outStream.Length);
                serverStream.Flush();
            }
            catch (Exception ex) {
                request_xml = "";
                message = ex.Message;
                return false;
            }

            request_xml = "";
            string returndata = "";

            if (!noreturn)
            {
                try
                {
                    int len = serverStream.Read(inStream, 0, 1000000);
                    returndata = Encoding.Unicode.GetString(inStream, 0, len);
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    return false;
                }

                XmlDocument doc = new XmlDocument();
                try
                {
                    doc.LoadXml(returndata);
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    return false;
                }

                message = doc.SelectNodes("//root/message")[0].InnerText;
            }
            else
                message = "OK";

            Int64 parsed = 0;
            hasrows = false;

            if (message != "OK" && Int64.TryParse(message, out parsed) == false)
                return false;

            if (message != "OK")
            {
                last_insert_id_or_affected_rows = parsed;
                message = "OK";
                return true;
            }
            else
                last_insert_id_or_affected_rows = 0;

            return true;
        }

        public SQLMachineDataReader execute_reader(String command) {
            var ret = new SQLMachineDataReader();

            request_xml = "<command>" + XmlEscape(command) + "</command>\n";
            request_xml += "<db>" + XmlEscape(db) + "</db>\n";
            request_xml += "<password>" + XmlEscape(password) + "</password>\n";

            request_xml += "</root>";
            request_xml = "<?xml version=\"1.0\" encoding=\"UTF-16\"?>\n<root>\n" + request_xml;

            reset();

            NetworkStream serverStream = null;

            try
            {
                serverStream = clientSocket.GetStream();
                byte[] outStream = Encoding.Unicode.GetBytes(request_xml);
                serverStream.Write(outStream, 0, outStream.Length);
                serverStream.Flush();
            }
            catch (Exception ex)
            {
                request_xml = "";
                ret.message = message = ex.Message;
                return ret;
            }

            request_xml = "";
            string returndata = "";

            try
            {
                int len = serverStream.Read(inStream, 0, 1000000);
                returndata = Encoding.Unicode.GetString(inStream, 0, len);
            }
            catch (Exception ex)
            {
                ret.message = message = ex.Message;
                return ret;
            }

            XmlDocument doc = new XmlDocument();
            try
            {
                doc.LoadXml(returndata);
            }
            catch (Exception ex)
            {
                ret.message = message = ex.Message;
                return ret;
            }

            message = doc.SelectNodes("//root/message")[0].InnerText;
            var rid = doc.SelectNodes("//root/rid");

            if (rid.Count > 0 && Int64.TryParse(rid[0].InnerText, out ret.recordset_id))
                ret.hasrows = true;
            else
            {
                ret.recordset_id = 0;
                ret.hasrows = false;
            }

            ret.db = db;
            ret.password = password;
            ret.port = port;
            ret.server_ip = server_ip;
            ret.message = message;
            ret.clientSocket = clientSocket;

            return ret;
        }
        private void reset()
        {
            message = "";
            hasrows = false;
            last_insert_id_or_affected_rows = 0;
        }
    }

    public class SQLMachineDataReader
    {
        public Int64 recordset_id;
        public String db, password, server_ip;
        public int port;
        public String request_xml = "";

        public List<String> colnames;
        public List<String> types;
        public List<String> record;
        public String message;
        public Boolean hasrows;

        private static byte[] inStream = new byte[1000000];
        public TcpClient clientSocket = null;

        public bool fetch_next_record()
        {
            var ret = new List<String>();
            request_xml = "<command>RECORDSET-FETCH-RESULT</command>\n";
            request_xml += "<recordset_id>" + XmlEscape(recordset_id.ToString()) + "</recordset_id>\n";

            request_xml += "<db>" + XmlEscape(db) + "</db>\n";
            request_xml += "<password>" + XmlEscape(password) + "</password>\n";

            request_xml += "</root>";
            request_xml = "<?xml version=\"1.0\" encoding=\"UTF-16\"?>\n<root>\n" + request_xml;

            reset();

            NetworkStream serverStream = null;

            try
            {
                serverStream = clientSocket.GetStream();
                byte[] outStream = Encoding.Unicode.GetBytes(request_xml);
                serverStream.Write(outStream, 0, outStream.Length);
                serverStream.Flush();
            }
            catch (Exception ex)
            {
                request_xml = "";
                message = ex.Message;
                return false;
            }

            request_xml = "";
            string returndata = "";

            try
            {
                int len = serverStream.Read(inStream, 0, 1000000);
                returndata = Encoding.Unicode.GetString(inStream, 0, len);
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return false;
            }

            XmlDocument doc = new XmlDocument();
            try
            {
                doc.LoadXml(returndata);
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return false;
            }

            message = doc.SelectNodes("//root/message")[0].InnerText;
            hasrows = doc.SelectNodes("//root/hasrows").Count > 0;

            if (hasrows)
            { 
                XmlNodeList recs0 = doc.SelectNodes("//root/records");
                XmlNodeList recs = recs0.Item(0).ChildNodes;

                XmlNodeList col = recs.Item(0).ChildNodes;
         
                for (int k = 0; k < col.Count; ++k)
                    if (col.Item(k).Attributes.Count > 0) // isnull="1" atrribute
                        record.Add(null);
                    else
                        record.Add(col.Item(k).InnerText);

                return true;
            }

            return false;
        }

        public bool fetch_fields()
        {
            var ret = new List<String>();
            request_xml = "<command>RECORDSET-FETCH-FIELDS</command>\n";
            request_xml += "<recordset_id>" + XmlEscape(recordset_id.ToString()) + "</recordset_id>\n";

            request_xml += "<db>" + XmlEscape(db) + "</db>\n";
            request_xml += "<password>" + XmlEscape(password) + "</password>\n";

            request_xml += "</root>";
            request_xml = "<?xml version=\"1.0\" encoding=\"UTF-16\"?>\n<root>\n" + request_xml;

            reset2();

            NetworkStream serverStream = null;

            try
            {
                serverStream = clientSocket.GetStream();
                byte[] outStream = Encoding.Unicode.GetBytes(request_xml);
                serverStream.Write(outStream, 0, outStream.Length);
                serverStream.Flush();
            }
            catch (Exception ex)
            {
                request_xml = "";
                message = ex.Message;
                return false;
            }

            request_xml = "";
            string returndata = "";

            try
            {
                int len = serverStream.Read(inStream, 0, 1000000);
                returndata = Encoding.Unicode.GetString(inStream, 0, len);
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return false;
            }

            XmlDocument doc = new XmlDocument();
            try
            {
                doc.LoadXml(returndata);
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return false;
            }

            message = doc.SelectNodes("//root/message")[0].InnerText;
            hasrows = doc.SelectNodes("//root/hasrows").Count > 0;

            if (message == "OK")
            {
                XmlNodeList cols0 = doc.SelectNodes("//root/records/r");
                XmlNodeList cols = cols0.Item(0).ChildNodes;

                for (int i = 0; i < cols.Count; ++i)
                    colnames.Add(cols.Item(i).InnerText);

                XmlNodeList typs0 = doc.SelectNodes("//root/records/r2");
                XmlNodeList typs = typs0.Item(0).ChildNodes;

                for (int i = 0; i < typs.Count; ++i)
                    types.Add(typs.Item(i).InnerText);

                return true;
            }

            return false;
        }

        public int num_rows()
        {
            request_xml = "<command>RECORDSET-NUM-ROWS</command>\n";
            request_xml += "<recordset_id>" + XmlEscape(recordset_id.ToString()) + "</recordset_id>\n";

            request_xml += "<db>" + XmlEscape(db) + "</db>\n";
            request_xml += "<password>" + XmlEscape(password) + "</password>\n";

            request_xml += "</root>";
            request_xml = "<?xml version=\"1.0\" encoding=\"UTF-16\"?>\n<root>\n" + request_xml;

            message = "";
            hasrows = false;

            NetworkStream serverStream = null;

            try
            {
                serverStream = clientSocket.GetStream();
                byte[] outStream = Encoding.Unicode.GetBytes(request_xml);
                serverStream.Write(outStream, 0, outStream.Length);
                serverStream.Flush();
            }
            catch (Exception ex)
            {
                request_xml = "";
                message = ex.Message;
                return -1;
            }

            request_xml = "";
            string returndata = "";

            try
            {
                int len = serverStream.Read(inStream, 0, 1000000);
                returndata = Encoding.Unicode.GetString(inStream, 0, len);
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return -1;
            }

            XmlDocument doc = new XmlDocument();
            try
            {
                doc.LoadXml(returndata);
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return -1;
            }

            message = doc.SelectNodes("//root/message")[0].InnerText;
            int i32;

            if (int.TryParse(message, out i32))
            {
                message = "OK";
                return i32;
            }

            return -1;
        }

        public bool move_first()
        {
            request_xml = "<command>RECORDSET-MOVE-FIRST</command>\n";
            request_xml += "<recordset_id>" + XmlEscape(recordset_id.ToString()) + "</recordset_id>\n";

            request_xml += "<db>" + XmlEscape(db) + "</db>\n";
            request_xml += "<password>" + XmlEscape(password) + "</password>\n";

            request_xml += "</root>";
            request_xml = "<?xml version=\"1.0\" encoding=\"UTF-16\"?>\n<root>\n" + request_xml;

            reset();

            NetworkStream serverStream = null;

            try
            {
                serverStream = clientSocket.GetStream();
                byte[] outStream = Encoding.Unicode.GetBytes(request_xml);
                serverStream.Write(outStream, 0, outStream.Length);
                serverStream.Flush();
            }
            catch (Exception ex)
            {
                request_xml = "";
                message = ex.Message;
                return false;
            }

            request_xml = "";
            string returndata = "";

            try
            {
                int len = serverStream.Read(inStream, 0, 1000000);
                returndata = Encoding.Unicode.GetString(inStream, 0, len);
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return false;
            }

            XmlDocument doc = new XmlDocument();
            try
            {
                doc.LoadXml(returndata);
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return false;
            }

            message = doc.SelectNodes("//root/message")[0].InnerText;
            
            if (message == "OK")
                return true;
            
            return false;
        }

        public bool close()
        {
            request_xml = "<command>RECORDSET-CLOSE</command>\n";
            request_xml += "<recordset_id>" + XmlEscape(recordset_id.ToString()) + "</recordset_id>\n";

            request_xml += "<db>" + XmlEscape(db) + "</db>\n";
            request_xml += "<password>" + XmlEscape(password) + "</password>\n";

            request_xml += "</root>";
            request_xml = "<?xml version=\"1.0\" encoding=\"UTF-16\"?>\n<root>\n" + request_xml;

            reset();

            NetworkStream serverStream = null;

            try
            {
                serverStream = clientSocket.GetStream();
                byte[] outStream = Encoding.Unicode.GetBytes(request_xml);
                serverStream.Write(outStream, 0, outStream.Length);
                serverStream.Flush();
            }
            catch (Exception ex)
            {
                request_xml = "";
                message = ex.Message;
                return false;
            }

            request_xml = "";
            string returndata = "";

            try
            {
                int len = serverStream.Read(inStream, 0, 1000000);
                returndata = Encoding.Unicode.GetString(inStream, 0, len);
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return false;
            }

            XmlDocument doc = new XmlDocument();
            try
            {
                doc.LoadXml(returndata);
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return false;
            }

            message = doc.SelectNodes("//root/message")[0].InnerText;

            if (message == "OK")
                return true;

            return false;
        }

        private void reset()
        {
            record = new List<String>();
            message = "";
            hasrows = false;
        }

        private void reset2()
        {
            colnames = new List<String>();
            types = new List<String>();
            message = "";
            hasrows = false;
        }

        private string XmlEscape(string unescaped)
        {
            XmlDocument doc = new XmlDocument();
            XmlNode node = doc.CreateElement("root");
            node.InnerText = unescaped;
            return node.InnerXml;
        }

        private string XmlUnescape(string escaped)
        {
            XmlDocument doc = new XmlDocument();
            XmlNode node = doc.CreateElement("root");
            node.InnerXml = escaped;
            return node.InnerText;
        }
    }
}
