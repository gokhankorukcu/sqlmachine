﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sqlim
{
    public partial class ShowData : Form
    {
        int ri, ci;
        DataGridView g;
        public ShowData(DataGridView g, int ri, int ci)
        {
            InitializeComponent();
            this.ri = ri;
            this.ci = ci;
            this.g = g;

            if (g.ReadOnly == true)
                button1.Enabled = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            g.Rows[ri].Cells[ci].Value = textBox1.Text;
            this.Close();
        }

        private void ShowData_Load(object sender, EventArgs e)
        {
            textBox1.Text = g.Rows[ri].Cells[ci].Value.ToString();
        }
    }
}
