﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

using SQLMachineCommandNS;

namespace Sqlim
{
    public partial class BackupDatabaseForm : Form
    {
        Form1 opener;
        public BackupDatabaseForm(Form1 opener)
        {
            InitializeComponent();
            this.opener = opener;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (opener.Conn == null)
            {
                MessageBox.Show("Open a database firstly!");
                return;
            }

            if (comboBox1.SelectedIndex < 0)
            {
                MessageBox.Show("Please specify backup type firstly!");
                return;
            }

            if (textBox1.Text.Length == 0)
            {
                MessageBox.Show("Please specify output backup file path firstly!");
                return;
            }

            if (MessageBox.Show("All previous data in specified backup file will be deleted!", "Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
            {
                File.WriteAllText(textBox1.Text, "", Encoding.Unicode);

                switch(comboBox1.SelectedIndex)
                {
                    case 0: backup_structure_and_data(textBox1.Text); break;
                    case 1: backup_structure(textBox1.Text); break;
                    case 2: backup_data(textBox1.Text); break;
                }

                MessageBox.Show("Backup process has finished.");
            }
        }

        private void backup_structure_and_data(String filename)
        {
            List<String> tables = new List<String>();

            var ret = opener.Conn.execute_reader("SHOW TABLES " + opener.database);

            while (ret.fetch_next_record())
            {
                tables.Add(ret.record[0]);
            }

            ret.close();

            if (tables.Count == 0) return;

            double progress_value_per_iteration = (double)progressBar1.Maximum / tables.Count;
            double progress_curr_value = 0;

            for (int i = 0; i < tables.Count; ++i)
            {
                var ret3 = opener.Conn.execute_reader("SHOW TABLE " + tables[i]);

                String str = "CREATE TABLE "+ tables[i] + " (";
                while (ret3.fetch_next_record())
                {
                    str += ret3.record[0] +",";
                }
                str = str.Substring(0, str.Length - 1);
                str += ");\n";

                File.AppendAllText(filename, str, Encoding.Unicode);

                var ret2 = opener.Conn.execute_reader("SHOW INDEXES " + tables[i]);
                List<String> indexes = new List<String>();
                Dictionary<String, List<String>> d_indexes = new Dictionary<String, List<String>>();

                while (ret2.fetch_next_record())
                {
                    indexes.Add(ret2.record[0]);
                }

                if (indexes.Count > 0) {
                    for (int k = 0; k < indexes.Count; ++k)
                    {
                        var pindex = indexes[k].Split(' ');
                        var indexname = pindex[0];
                       
                        var ret4 = opener.Conn.execute_reader("SHOW INDEX " + indexname);
                        List<String> indexdefs = new List<String>();
                        while (ret4.fetch_next_record())
                        {
                            indexdefs.Add(ret4.record[0]);
                        }
                        d_indexes.Add(indexname, indexdefs);
                    }

                    for (int k = 0; k < indexes.Count; ++k)
                    {
                        var pindex = indexes[k].Split(' ');
                        var indexname = pindex[0];
                        var unique = pindex.Length > 1 && pindex[1] == "UNIQUE" ? "UNIQUE " : "";

                        if (indexname.Substring(0, 1) == "_")
                            continue;

                        str = "CREATE " + unique + "INDEX " + indexname + " ON " + tables[i] + " (";
                        for (int j = 0; j < d_indexes[indexname].Count; ++j) {
                            var pa = d_indexes[indexname][j].Split(' ');
                            str += pa[1] + ",";
                        }

                        str = str.Substring(0, str.Length - 1);
                        str += ");\n";

                        File.AppendAllText(filename, str, Encoding.Unicode);
                    }
                }

                var ret5 = opener.Conn.execute_reader("SELECT * FROM " + tables[i]);
                ret5.fetch_fields();

                while (ret5.fetch_next_record())
                {
                    str = "INSERT INTO " + tables[i] + " (";
                    for (int k = 0; k < ret5.colnames.Count; ++k)
                    {
                        str += ret5.colnames[k] + ",";
                    }
                        
                    str = str.Substring(0, str.Length - 1);

                    str += ") VALUES(";

                    for (int k = 0; k < ret5.colnames.Count; ++k)
                    {
                        var val = ret5.record[k] == null ? "NULL" : "'" + ret5.record[k] + "'";
                        str += val + ",";
                    }
                    str = str.Substring(0, str.Length - 1);

                    str += ");\n";

                    File.AppendAllText(filename, str, Encoding.Unicode);
                }

                progress_curr_value += progress_value_per_iteration;
                progressBar1.Value = (int)progress_curr_value;
                Application.DoEvents();
            }
        }
        private void backup_structure(String filename)
        {
            List<String> tables = new List<String>();

            var ret = opener.Conn.execute_reader("SHOW TABLES " + opener.database);

            while (ret.fetch_next_record())
            {
                tables.Add(ret.record[0]);
            }

            ret.close();

            if (tables.Count == 0) return;

            double progress_value_per_iteration = (double)progressBar1.Maximum / tables.Count;
            double progress_curr_value = 0;

            for (int i = 0; i < tables.Count; ++i)
            {
                var ret3 = opener.Conn.execute_reader("SHOW TABLE " + tables[i]);

                String str = "CREATE TABLE " + tables[i] + " (";
                while (ret3.fetch_next_record())
                {
                    str += ret3.record[0] + ",";
                }
                str = str.Substring(0, str.Length - 1);
                str += ");\n";

                File.AppendAllText(filename, str, Encoding.Unicode);

                var ret2 = opener.Conn.execute_reader("SHOW INDEXES " + tables[i]);
                List<String> indexes = new List<String>();
                Dictionary<String, List<String>> d_indexes = new Dictionary<String, List<String>>();

                while (ret2.fetch_next_record())
                {
                    indexes.Add(ret2.record[0]);
                }

                if (indexes.Count > 0)
                {
                    for (int k = 0; k < indexes.Count; ++k)
                    {
                        var pindex = indexes[k].Split(' ');
                        var indexname = pindex[0];

                        var ret4 = opener.Conn.execute_reader("SHOW INDEX " + indexname);
                        List<String> indexdefs = new List<String>();
                        while (ret4.fetch_next_record())
                        {
                            indexdefs.Add(ret4.record[0]);
                        }
                        d_indexes.Add(indexname, indexdefs);
                    }

                    for (int k = 0; k < indexes.Count; ++k)
                    {
                        var pindex = indexes[k].Split(' ');
                        var indexname = pindex[0];
                        var unique = pindex.Length > 1 && pindex[1] == "UNIQUE" ? "UNIQUE " : "";

                        if (indexname.Substring(0, 1) == "_")
                            continue;

                        str = "CREATE " + unique + "INDEX " + indexname + " ON " + tables[i] + " (";
                        for (int j = 0; j < d_indexes[indexname].Count; ++j)
                        {
                            var pa = d_indexes[indexname][j].Split(' ');
                            str += pa[1] + ",";
                        }

                        str = str.Substring(0, str.Length - 1);
                        str += ");\n";

                        File.AppendAllText(filename, str, Encoding.Unicode);
                    }
                }

                progress_curr_value += progress_value_per_iteration;
                progressBar1.Value = (int)progress_curr_value;
                Application.DoEvents();
            }
        }
        private void backup_data(String filename)
        {
            List<String> tables = new List<String>();

            var ret = opener.Conn.execute_reader("SHOW TABLES " + opener.database);

            while (ret.fetch_next_record())
            {
                tables.Add(ret.record[0]);
            }

            ret.close();

            if (tables.Count == 0) return;

            double progress_value_per_iteration = (double)progressBar1.Maximum / tables.Count;
            double progress_curr_value = 0;

            for (int i = 0; i < tables.Count; ++i)
            {
                String str;
                var ret5 = opener.Conn.execute_reader("SELECT * FROM " + tables[i]);
                ret5.fetch_fields();

                while (ret5.fetch_next_record())
                {
                    str = "INSERT INTO " + tables[i] + " (";
                    for (int k = 0; k < ret5.colnames.Count; ++k)
                    {
                        str += ret5.colnames[k] + ",";
                    }

                    str = str.Substring(0, str.Length - 1);

                    str += ") VALUES(";

                    for (int k = 0; k < ret5.colnames.Count; ++k)
                    {
                        var val = ret5.record[k] == null ? "NULL" : "'" + ret5.record[k] + "'";
                        str += val + ",";
                    }
                    str = str.Substring(0, str.Length - 1);

                    str += ");\n";

                    File.AppendAllText(filename, str, Encoding.Unicode);
                }

                progress_curr_value += progress_value_per_iteration;
                progressBar1.Value = (int)progress_curr_value;
                Application.DoEvents();
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
                textBox1.Text = openFileDialog1.FileName;
        }
    }
}
