﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Collections;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Data.SQLite;
using SQLMachineCommandNS;

namespace Sqlim
{
    public partial class Form1 : Form
    {
        public String database, username="", password="", server="", port="";
        public TreeView tv;
        public SQLMachineCommand Conn;
        public SQLMachineCommand Conn2;
        public Boolean shutdowned = false, db_dropped = false;
        private int g_indeksler_count = 0;

        public SQLiteConnection sconn = new SQLiteConnection("Data Source=SQLMachine-MS-CONF.sqlite;");

        public object[][] g_rows;
        public String gecerli_tablo="", gecerli_engine, gecerli_collation;
        public int gecerli_indis;

        public object[][] g_tablolar;
        public object[][] g_collations;

        public List<List<object>> g_indeksler = new List<List<object>>();
        public List<List<object>> g_foreigns = new List<List<object>>();

        private int curr_ai_column_indis = -1;
        private List<String> curr_colnames;

        public Boolean program = true;

        public Form1()
        {
            InitializeComponent();
            tv = treeView1;
            sconn.Open();
        }

        private void dosyaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void openDatabaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Conn != null)
            {
                MessageBox.Show("Close active database firstly!");
                return;
            }

            Form2 f = new Form2(this);
            f.ShowDialog();

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void treeView1_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (treeView1.SelectedNode == null || treeView1.SelectedNode.Parent == null)
                return;

            String t = treeView1.SelectedNode.Parent.Text;

            if (t != "Tables")
                return;

            if (treeView1.SelectedNode.Parent.Text == "Tables")
            {
                //tabControl1.SelectedIndex = 0;
                dataGridView3.CellValueChanged -= dataGridView3_CellValueChanged;

                yapi_getir(treeView1.SelectedNode.Text);

                dataGridView3.CellValueChanged += dataGridView3_CellValueChanged;

                gecerli_tablo = treeView1.SelectedNode.Text;
                label1.Text = "Table : " + gecerli_tablo;
            }
        }

        private void show_create(String parent, String text)
        {
            
        }

        public void yapi_getir(String tablename) {
            var ret = Conn.execute_reader("SHOW TABLE " + tablename);


            int count = ret.num_rows();
       

            g_rows = new object[count][];

            

            
            dataGridView1.Rows.Clear();
            

            
                for (var i = 0; ret.fetch_next_record(); ++i)
                {
                    object[] p = new object[4];
                    g_rows[i] = new object[4];

                    var parcalar = ret.record[0].Split(' ');

                    p[0] = parcalar[0];
                   
                    
                    p[1] = parcalar[1];
                    
                    p[2] = parcalar.Contains("IDENTITY") ? "1" : "0";
                    p[3] = parcalar.Contains("CI") ? "1" : "0";

                    dataGridView1.Rows.Insert(i, p);
                    p.CopyTo(g_rows[i], 0);
                }
                

                indexleri_getir(tablename);
                
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Conn != null) Conn.close_connection();
            if (Conn2 != null) Conn2.close_connection();
            sconn.Close();
        }

        private void hakkındaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox1 f = new AboutBox1();
            f.ShowDialog();
        }

        private void tabloOluşturToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Conn == null)
            {
                MessageBox.Show("Open a database firstly!");
                return;
            }
            Form3 f = new Form3(this, Conn);
            f.ShowDialog();
        }

        private String ks(Object p)
        {
            if (p == null) return "";
            return p.ToString();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        

        private void button1_Click(object sender, EventArgs e)
        {
           
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (gecerli_tablo == "")
                return;

            if (MessageBox.Show("Do you really want to drop `" + gecerli_tablo + "` table?\n\nWarning : All data will be lost for the this table!", "Warning", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {
                tablo_sil(gecerli_tablo);
                gecerli_tablo = gecerli_collation = gecerli_engine = "";
               
                dataGridView1.Rows.Clear();
                dataGridView2.Rows.Clear();
                dataGridView3.DataSource = null;
                label1.Text = "";
            }
        }

        private void tablo_sil(String tablo)
        {
            Conn.execute("DROP TABLE " + tablo);

            tree_guncelle();
        }

       

        private void db_sil()
        {

            Conn.execute("DROP DATABASE "+ database);

            //sconn.Close();
            dataGridView1.Rows.Clear();
            dataGridView2.Rows.Clear();
            //dataGridView3.Rows.Clear();

            treeView1.Nodes.Clear();
            
            dataGridView3.DataSource = null;
            label1.Text = "";

            gecerli_tablo = gecerli_engine = gecerli_collation = "";
        }

        public void tree_guncelle()
        {
            TreeNode rn = treeView1.Nodes[0];
            TreeNode tables = rn.Nodes["Tables"];
            
            tables.Nodes.Clear();
            
            g_tablolar = null;

            if (Conn == null)
            {
                treeView1.Nodes.Clear();
                return;
            }

            List<List<String>> names = new List<List<String>>();

            var ret = Conn.execute_reader("SHOW TABLES "+ database);

            while (ret.fetch_next_record()) {
                List<String> prc = new List<String>();
                prc.Add(ret.record[0]);
                names.Add(prc);
            }
           

            if (names.Count > 0)
                g_tablolar = new object[names.Count][];

            TreeNode tn;

            for (int i = 0; i < names.Count; ++i)
            {
                g_tablolar[i] = new object[3];
                g_tablolar[i][0] = names[i][0];
                

                tn = tables.Nodes.Add(names[i][0], names[i][0]);
                tn.ImageIndex = 3;
                tn.SelectedImageIndex = 3;
            }
        }

        

        public void indexleri_getir(String tablename)
        {
            DataGridViewComboBoxColumn theColumn = (DataGridViewComboBoxColumn)dataGridView2.Columns["IndexType"];

            if (theColumn.Items.Count == 0)
            {
                theColumn.Items.Add("");
                theColumn.Items.Add("UNIQUE");
            }

            DataGridViewButtonColumn bc = (DataGridViewButtonColumn)dataGridView2.Columns["Detail"];
            bc.Text = "Detail";

            var ret = Conn.execute_reader("SHOW INDEXES "+ tablename);

            g_indeksler.Clear();
            dataGridView2.Rows.Clear();


            for (var i = 0; ret.fetch_next_record(); ++i)
            {
                object[] p = new object[4];
                var parcalar = ret.record[0].Split(' ');
                var cols = "";

                var ret2 = Conn2.execute_reader("SHOW INDEX " + parcalar[0]);
                for (var k = 0; ret2.fetch_next_record(); ++k)
                {
                    if (k != 0)
                        cols += ",";
                    var pa = ret2.record[0].Split(' ');
                    cols +=  pa[1];
                }


                p[0] = parcalar[0];
                p[1] = cols;
                p[2] = "Detail";
                p[3] = parcalar.Contains("UNIQUE") ? "UNIQUE" : "";
                
                List<object> satir = p.ToList();

                
                g_indeksler.Add(satir);
                g_indeksler_count = g_indeksler.Count;
                

                dataGridView2.Rows.Insert(i, p);
                
            }
            
        }

        public void foreign_getir(String tablename)
        {
           
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (Conn == null)
            {
                MessageBox.Show("Open a database firstly!");
                return;
            }

            bool error = false;

            for (int i = 0; i < dataGridView2.RowCount; ++i) {
                String iname = ks(dataGridView2.Rows[i].Cells["IndexName"].Value);
                String cols = ks(dataGridView2.Rows[i].Cells["Columns"].Value);
                String itype = ks(dataGridView2.Rows[i].Cells["IndexType"].Value);

                if (iname == "" || cols == "" || gecerli_tablo == "")
                    continue;

                
                if (i >= g_indeksler_count)
                {
                    Conn.execute("CREATE"+ (itype == "UNIQUE"?" UNIQUE":"") +" INDEX "+ iname +" ON "+ gecerli_tablo +" ("+ cols +")");
                    if (Conn.message != "OK")
                    {
                        MessageBox.Show((i+1) + ". index creation raises exception: Exception message is : " + Conn.message + " If any, previous index(es) has been created. The row that raises exception error and row(s) that come after this error will be deleted now!");
                        error = true;
                        break;
                    }
                }
            }

            indexleri_getir(gecerli_tablo);
            if (!error)
                MessageBox.Show("All insert(s) have been completed.");
            else
                MessageBox.Show("Insert(s) have been partially completed!"); 
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (gecerli_tablo == "")
            {
                MessageBox.Show("Please open a table firstly!");
                return;
            }

            if (e.ColumnIndex == 2)
            {
                IndexDetail f = new IndexDetail(this, e.RowIndex);
                f.ShowDialog();
            }
        }

        private bool delete_index(int row_index) {
            
            return false;
        }

        private bool delete_foreign(int row_index)
        {
            return false;
        }

        private void ındexOluşturToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Conn == null)
            {
                MessageBox.Show("Open a database firstly!");
                return;
            }
            tabControl1.SelectedIndex = 1;
        }

        private void createForeignKeyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void dataGridView3_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void dataGridView3_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void button6_Click(object sender, EventArgs e)
        {
            
        }

        

        private String Query_Click(String sql)
        {
            
                
            program = false;
            return "";
        }

        private Boolean Update_Click()
        {
            return false;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            
        }

        private void listBox1_DoubleClick(object sender, EventArgs e)
        {
            
        }


        public void closedatabase()
        {
            dataGridView1.Rows.Clear();
            dataGridView2.Rows.Clear();
            //dataGridView3.Rows.Clear();

            treeView1.Nodes.Clear();
            
            
            dataGridView3.DataSource = null;

            gecerli_tablo = gecerli_engine = gecerli_collation = "";
            if (Conn != null) Conn.close_connection();
            if (Conn2 != null) Conn2.close_connection();
            Conn = Conn2 = null;
        }

        private void closeDatabseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Conn == null)
            {
                MessageBox.Show("Open a database firstly!");
                return;
            }
            closedatabase();
            
        }

        private void createDatabaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Conn == null)
            {
                MessageBox.Show("Open a database firstly!");
                return;
            }
            CreateDatabase f = new CreateDatabase(this);
            f.ShowDialog();
        }

        private void treeView1_KeyDown(object sender, KeyEventArgs e)
        {
            TreeNode sn = treeView1.SelectedNode;
            if (sn == null)
                return;

            if (sn.Parent == null) {
                if (MessageBox.Show("Do you really want to delete `" + database + "` database?\n\nWarning : All data of this database will be lost!", "Confirm", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                    db_sil();        
            } else if (sn.Parent.Text == "Tables") {
                if (MessageBox.Show("Do you really want to delete `"+ sn.Text +"` table?\n\nWarning : All data of this table will be lost!", "Confirm", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                    tablo_sil(sn.Text);
                    dataGridView1.Rows.Clear();
                    dataGridView2.Rows.Clear();
                    dataGridView3.DataSource = null;
                    label1.Text = "";
            }
            
        }

        private void createViewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void functionOluşturToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void triggerOluşturToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void eventOluşturToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void button8_Click(object sender, EventArgs e)
        {
            
        }

        private void dataGridView4_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void button8_Click_1(object sender, EventArgs e)
        {
            
            
        }

        private void dataGridView3_CellValueChanged_1(object sender, DataGridViewCellEventArgs e)
        {
            if (curr_ai_column_indis == -1) {
                MessageBox.Show("To update column, an IDENTITY column must exist!");
                return;
            }

            if (curr_ai_column_indis == e.ColumnIndex)
            {
                MessageBox.Show("IDENTITY column can't be updated!");
                return;
            }
            
            String ai_value = dataGridView3.Rows[e.RowIndex].Cells[curr_ai_column_indis].Value.ToString();
            String new_value = dataGridView3.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
            String ai_name = curr_colnames[curr_ai_column_indis];
            String new_name = curr_colnames[e.ColumnIndex];

            Conn.execute("UPDATE "+ gecerli_tablo +" SET "+ new_name +" = '"+ new_value + "' WHERE " + ai_name + " = " + ai_value);

            if (Conn.message == "OK")
                MessageBox.Show("Row has just updated.");
            else
                MessageBox.Show(Conn.message);
        }

        private void dataGridView3_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            
        }

        private void dataGridView3_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void ımportBackupFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var f = new BackupDatabaseForm(this);
            f.Show();
        }

        private void loadSQLFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("All data in textbox will be deleted!", "Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
            {
                DialogResult result = openFileDialog1.ShowDialog();
                if (result == DialogResult.OK)
                {
                    string file = openFileDialog1.FileName;
                    fastColoredTextBox1.Text = File.ReadAllText(file);
                }
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            if (Conn == null) {
                MessageBox.Show("Please open a database firstly!");
                return;
            }

            if (shutdowned)
            {
                MessageBox.Show("This program has shutdowned and your query or queries will not be sent!");
                return;
            }

            if (fastColoredTextBox1.Text.Length > 0) { 
                string[] p = Regex.Split(fastColoredTextBox1.Text, ";(?=(?:[^']*'[^']*')*[^']*$)");

                if (p.Length == 1) {
                    string statement = p[0].Trim();
                    if (statement.Length > 0) {
                        if (true)
                        {
                            dataGridView4.DataSource = null;

                            SQLMachineDataReader ret = new SQLMachineDataReader();
                            ret.hasrows = false;
                            String message = "";

                            if (statement.Length >= 13 && String.Equals(statement.Substring(0,13), "DROP DATABASE", StringComparison.OrdinalIgnoreCase)) {
                                Conn.execute(statement);

                                message = Conn.message;
                                if (message == "OK")
                                {
                                    db_dropped = true;
                                    Conn.close_connection();
                                    Conn = null;
                                    Conn2.close_connection();
                                    Conn2 = null;
                                }                     
                            }
                            else if (String.Equals(statement, "SHUTDOWN", StringComparison.OrdinalIgnoreCase))
                            {
                                Conn.execute(statement, true);

                                message = Conn.message;
                                if (message == "OK")
                                {
                                    shutdowned = true;
                                    Conn.close_connection();
                                    Conn = null;
                                    Conn2.close_connection();
                                    Conn2 = null;
                                }

                            }
                            else
                            {
                                ret = Conn.execute_reader(statement);
                                message = Conn.message;
                            }

                            tree_guncelle();
                            MessageBox.Show(message);

                            if (!ret.hasrows) {
                                dataGridView4.DataSource = null;
                                return;
                            }

                            if (shutdowned == false && db_dropped == false)
                            {
                                ret.fetch_fields();

                                DataTable table = new DataTable();
                                for (var i = 0; i < ret.colnames.Count; ++i)
                                {
                                    table.Columns.Add(ret.colnames[i]);
                                }

                                while (ret.fetch_next_record())
                                {
                                    DataRow row = table.NewRow();

                                    for (var i = 0; i < ret.record.Count; ++i)
                                    {
                                        row.SetField(i, ret.record[i]);
                                    }
                                    table.Rows.Add(row);
                                }

                                dataGridView4.DataSource = table;
                            }
                        }
                    }
                }
                else
                {
                    var f = new OutputForm();
                    f.Show();

                    for (int i = 0; i < p.Length; ++i)
                    {
                        p[i] = p[i].Trim();
                        if (p[i].Length == 0)
                            continue;

                        bool ret;

                        if (String.Equals(p[i], "SHUTDOWN", StringComparison.OrdinalIgnoreCase))
                            shutdowned = true;

                        if (p[i].Length >= 13 && String.Equals(p[i].Substring(0, 13), "DROP DATABASE", StringComparison.OrdinalIgnoreCase))
                            db_dropped = true;

                        Conn.execute(p[i], shutdowned);

                        f.textBox1.Text += (i + 1) + ". STATEMENT'S RETURN VALUE IS : " + Conn.message + "\r\n";
                        if (shutdowned && Conn.message == "OK")
                        {
                            f.textBox1.Text += "SHUTDOWN command sent and statement(s)'s execution interrupted!";
                            Conn.close_connection();
                            Conn = null;
                            Conn2.close_connection();
                            Conn2 = null;
                        }
                        else if (db_dropped && Conn.message == "OK")
                        {
                            f.textBox1.Text += "DROP DATABASE command sent and statement(s)'s execution interrupted!";
                            Conn.close_connection();
                            Conn = null;
                            Conn2.close_connection();
                            Conn2 = null;
                        }
                        else
                            db_dropped = false;

                        tree_guncelle();

                        f.textBox1.Refresh();
                        Application.DoEvents();
                        if (shutdowned || db_dropped)
                            break;
                    }

                    MessageBox.Show("All statements have been executed.");
                }
            }
        }

        private void dataGridView3_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int row = dataGridView3.HitTest(e.X, e.Y).RowIndex;
            int column = dataGridView3.HitTest(e.X, e.Y).ColumnIndex;

            if (row >= 0 && column >= 0) {
                var f = new ShowData(dataGridView3, row, column);
                f.ShowDialog();
            }
        }

        private void dataGridView4_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView4_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int row = dataGridView4.HitTest(e.X, e.Y).RowIndex;
            int column = dataGridView4.HitTest(e.X, e.Y).ColumnIndex;

            if (row >= 0 && column >= 0)
            {
                var f = new ShowData(dataGridView4, row, column);
                f.ShowDialog();
            }
        }

        private void dataGridView4_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            
        }

        private void dataGridView4_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            
        }

        private void ımportFromFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void exportToFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            

            
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (Conn == null)
            {
                MessageBox.Show("Open a database firstly!");
                return;
            }

            var ret = Conn.execute_reader("SELECT * FROM "+ gecerli_tablo +" LIMIT "+ textBox1.Text +", "+ textBox2.Text);

            if (ret.message != "OK")
            {
                MessageBox.Show(Conn.message);
                return;
            }

            ret.fetch_fields();

            curr_colnames = ret.colnames;

            curr_ai_column_indis = -1;

            DataTable table = new DataTable();
            for (var i = 0; i < ret.colnames.Count; ++i) {
                table.Columns.Add(ret.colnames[i]);
                var p = ret.types[i].Split(',');
                if (p.Length > 1 && p[1] == "AI")
                    curr_ai_column_indis = i;
            }

            while (ret.fetch_next_record())
            {
                DataRow row = table.NewRow();
                
                for (var i = 0; i < ret.record.Count; ++i)
                {
                    row.SetField(i, ret.record[i]);
                }
                table.Rows.Add(row);
            }

            dataGridView3.DataSource = table;
        }
    }
}
